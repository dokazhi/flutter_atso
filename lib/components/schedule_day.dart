import 'package:flutter/material.dart';
class ScheduleCourse extends StatelessWidget {
  final courseName;
  final courseTimeBegin;
  final courseTimeEnd;
  final courseTeacher;
  final courseAuditory;
  final courseType;

  ScheduleCourse({
    this.courseName,
    this.courseTimeBegin,
    this.courseTimeEnd,
    this.courseTeacher,
    this.courseAuditory,
    this.courseType
  });

  final courseTypes = [
    'Лекция',
    'Семинар'
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border(
          top: BorderSide(width: 1, color: Colors.black12)
        )
      ),
      child: ListTile(
        dense: true,
        title: Text(courseName),
        leading: Column(

          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(bottom: 5.0),
              child: Text('$courseTimeBegin-$courseTimeEnd'),
            ),
            Text(courseTypes[courseType])
          ],
        ),
        subtitle: Text(courseTeacher),
        trailing: Text(courseAuditory.toString()),
      ),
    );
  }
}

class ScheduleDay extends StatelessWidget {
  final scheduleWeekDay;
  final double height;
  final double width;
  var _scheduleCourses = [
    {
      'courseName': 'Высшая математика, Дискретная математика, физикоматематика в создании видео игр ',
      'courseTimeBegin': '08:00',
      'courseTimeEnd': '08:50',
      'courseTeacher': 'проф. Ташева А.А.',
      'courseAuditory': 203,
      'courseType': 1
    },
    {
      'courseName': 'Исскуственный интеллект',
      'courseTimeBegin': '09:00',
      'courseTimeEnd': '09:50',
      'courseTeacher': 'проф. Ташева А.А.',
      'courseAuditory': 203,
      'courseType': 0
    },
    {
      'courseName': 'Численные методы',
      'courseTimeBegin': '10:00',
      'courseTimeEnd': '10:50',
      'courseTeacher': 'проф. Ташева А.А.',
      'courseAuditory': 203,
      'courseType': 1
    },
    {
      'courseName': 'Разработка веб-приложений с помощью Visual Studio',
      'courseTimeBegin': '11:00',
      'courseTimeEnd': '11:50',
      'courseTeacher': 'проф. Ташева А.А.',
      'courseAuditory': 203,
      'courseType': 0
    },
    {
      'courseName': '3D моделирование с помощью Blender',
      'courseTimeBegin': '12:00',
      'courseTimeEnd': '12:50',
      'courseTeacher': 'доц. Искакова А.К.',
      'courseAuditory': 203,
      'courseType': 1
    },
    {
      'courseName': 'Основы алгоритмизации',
      'courseTimeBegin': '13:00',
      'courseTimeEnd': '13:50',
      'courseTeacher': 'проф. Ташева А.А.',
      'courseAuditory': 203,
      'courseType': 1
    },

  ];
  ScheduleDay({
    this.scheduleWeekDay,
    this.height,
    this.width
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(bottom:20.0),
          child: Text(scheduleWeekDay, style: TextStyle(fontWeight: FontWeight.bold),),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom:8.0),
          child: Container(

            child: SingleChildScrollView(
              child: ListView.builder(
                shrinkWrap: true,
                physics: ClampingScrollPhysics(),
                itemCount: _scheduleCourses.length,
                itemBuilder: (BuildContext context, int index){
                  return Padding(
                    padding: const EdgeInsets.all(0.0),
                    child: ScheduleCourse(
                      courseName: _scheduleCourses[index]['courseName'],
                      courseTimeBegin: _scheduleCourses[index]['courseTimeBegin'],
                      courseTimeEnd: _scheduleCourses[index]['courseTimeEnd'],
                      courseTeacher: _scheduleCourses[index]['courseTeacher'],
                      courseAuditory: _scheduleCourses[index]['courseAuditory'],
                      courseType: _scheduleCourses[index]['courseType'],
                    ),
                  );
                }
              ),
            ),
      ),
        ),
      ],
    );
  }
}
