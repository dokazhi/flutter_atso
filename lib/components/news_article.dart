import 'package:atso/pages/news_detail_article.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';


class Article extends StatelessWidget {

  final articleId;
  final articleTitle;
  final articlePicture;
  final articleShortDescription;
  final articleDescription;
  final articleDate;

  Article({
    this.articleId,
    this.articleTitle,
    this.articlePicture,
    this.articleShortDescription,
    this.articleDescription,
    this.articleDate
  });

  @override
  Widget build(BuildContext context) {
    return Hero(
        tag: articleId.toString(),
        child: Container(
          padding: const EdgeInsets.all(10.0),
          decoration: BoxDecoration(
//              border: Border.all(color: Colors.blueAccent)
          ),
          child: Material(
            child: InkWell(
              onTap: (){
                Navigator.of(
                    context
                ).push(
                    new MaterialPageRoute(

                        builder: (context) => NewsDetailPage(
                            articleId: articleId,
                            articleTitle: articleTitle,
                            articleDescription: articleDescription,
                            articleDate: articleDate,
                            articlePicture: articlePicture,
                        )
                    )
                );

              },
              highlightColor: Colors.blue,
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: GridTile(
                  footer: Container(
                    color: Colors.white,
                    child: ListTile(
                      title: Text(articleTitle),
                      subtitle: Text(articleShortDescription),
                      trailing: Text(articleDate),
                    ),
                  ),
                  child: Image.asset(articlePicture, fit: BoxFit.cover,),
                ),
              ),
            ),

          ),
        )
    );
  }
}
