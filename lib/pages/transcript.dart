import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';


class TranscriptPage extends StatelessWidget {
  final courses;
  TranscriptPage({
    this.courses,
  });


  List<Widget> generateTabBars(){
    List<Widget> result = [];
    for(int i=0; i < courses.length;i++){
      result.add(
        Tab(text: '${i+1}')
      );
    }
    return result;
  }

  ListView generateSingleCoursePage(items, avgGPA){
    List<Widget> result = [
      ListTile(
        title: Text('Средний GPA за семестр'),
        trailing: Text('$avgGPA'),
      ),
      Divider(
        thickness: 1.0,
        color: Colors.black38,
      )
    ];
    for(int i =0; i < items.length; i++){

      result.add(
          ListTile(
            title: Text(items[i]['name']),
            trailing: Text('${items[i]["gpa"]} / ${items[i]["score"]}'),
          )
      );
    }
    return ListView(
      children: result,
    );
  }

  List<Widget> generateTabBodies(){
    List<Widget> result = [];
    for(int i=0; i < courses.length; i++){
      result.add(
        generateSingleCoursePage(courses[i]['items'], courses[i]['avgGPA'])
      );

    }
    return result;
  }
  @override
   Widget build(BuildContext context) {

     return DefaultTabController(
            length: courses.length,
            child: Scaffold(
              appBar: AppBar(
                bottom: TabBar(

                  tabs: generateTabBars(),
                ),
                title: Text('Транскрипт по семестрам'),
                backgroundColor: Colors.blueGrey,
     ),
              body: TabBarView(
                children: generateTabBodies(),
              ),
     ),
     );
   }
 }
