import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:indexed_list_view/indexed_list_view.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';
import 'package:fluttertoast/fluttertoast.dart';


class ExamItem extends StatelessWidget {
  final dateBegin;
  final dateEnd;
  final score;
  final itemName;
  final itemTeacherName;

  ExamItem({
    this.dateBegin,
    this.dateEnd,
    this.score,
    this.itemName,
    this.itemTeacherName
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(
          child: ListTile(
            leading: Padding(
              padding: const EdgeInsets.only(top:8.0),
              child: Column(
                children: <Widget>[
                  Text('с $dateBegin'),
                  Text('до $dateEnd')
                ],
              ),
            ),
            title: Text(itemName),
            subtitle: Text(itemTeacherName),
            trailing: Text(score),
          ),
        ),

          Icon((score != null) ? Icons.event_available : Icons.event_busy),

      ],
    );
  }
}




class Exams extends StatefulWidget {
  MediaQueryData mediaQueryData;
  Exams({
    this.mediaQueryData
  });

  @override
  _ExamsState createState() => _ExamsState(mediaQueryData: mediaQueryData);
}


class _ExamsState extends State<Exams> {
  MediaQueryData mediaQueryData;
  _ExamsState({
    this.mediaQueryData
  });
  final ItemScrollController itemScrollController = ItemScrollController();
  ScrollController controller;
  bool reversed = false;
  List<int> items1 = List.generate(50, (index) => index*2);
  List<int> items2 = List.generate(50, (index) => index*3);
  List<int> items3 = List.generate(50, (index) => index*4);

  List midtermList = [
    {
      'dateEnd': '2020-02-30',
      'dateBegin': '2020-02-23',
      'score': '90',
      'itemName': 'Высшая математика',
      'itemTeacherName': 'Белагор А.Г.'
    },
    {
      'dateEnd': '2020-02-30',
      'dateBegin': '2020-02-23',
      'score': '75',
      'itemName': 'Психология',
      'itemTeacherName': 'Автогор А.Г.'
    },
    {
      'dateEnd': '2020-02-30',
      'dateBegin': '2020-02-23',
      'score': '95',
      'itemName': 'Теория вероятности',
      'itemTeacherName': 'Фрйуд А.Г.'
    },
    {
      'dateEnd': '2020-02-30',
      'dateBegin': '2020-02-23',
      'score': '91',
      'itemName': 'Математическая статистика',
      'itemTeacherName': 'Змуйд Ф.Г.'
    },
    {
      'dateEnd': '2020-02-30',
      'dateBegin': '2020-02-23',
      'score': '96',
      'itemName': 'Моделирование',
      'itemTeacherName': 'Брасмутаоварвоа А.Г.'
    },
    {
      'dateEnd': '2020-02-30',
      'dateBegin': '2020-02-23',
      'score': '60',
      'itemName': 'Основы алгоритмизации и программирования',
      'itemTeacherName': 'Белагор А.Г.'
    },
  ];

  MediaQueryData md;
  Widget currentWidget;

  @override
  void initState() {
    // TODO: implement initState
    currentWidget = getMidterm();
    super.initState();
    controller = ScrollController();
  }



  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text('Контроль'),
        backgroundColor: Colors.blueGrey,
      ),
      floatingActionButton: SpeedDial(
          animatedIcon:  AnimatedIcons.menu_close,
          backgroundColor: Colors.blueGrey,
          curve: Curves.easeInCirc,
          children: [
            SpeedDialChild(
              child: Icon(Icons.event_note),
              label: 'Рубежные контроли',
              backgroundColor: Colors.blueGrey,
              onTap: (){
                setState(() {
                  currentWidget = getExams();
                });
              },
            ),
            SpeedDialChild(
              child: Icon(Icons.event_note),
              label: 'Экзамены',
              backgroundColor: Colors.blueGrey,
              onTap: (){
                setState(() {
                  currentWidget = getMidterm();
                });
              },
            ),
            SpeedDialChild(
              child: Icon(Icons.event_note),
              label: 'Курсовые/Лабораторные',
              backgroundColor: Colors.blueGrey,
              onTap: (){
                setState(() {
                  currentWidget = getLabWorks();
                });
              },
            ),
            SpeedDialChild(
              child: Icon(Icons.event_note),
              label: 'Практики',
              backgroundColor: Colors.blueGrey,
              onTap: (){
                setState(() {
                  currentWidget = getLabWorks();
                });
              },
            ),
          ]
      ),
      body: currentWidget,

    );
  }

  Widget getMidterm(){
    return Container(
          height: mediaQueryData.size.height * 0.9,

          child: ListView.builder(
          controller: controller,
            itemCount: midtermList.length,
            itemBuilder: (BuildContext context, int index){
              return Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: ExamItem(
                    dateBegin: midtermList[index]['dateBegin'],
                    dateEnd: midtermList[index]['dateEnd'],
                    score: midtermList[index]['score'],
                    itemName: midtermList[index]['itemName'],
                    itemTeacherName: midtermList[index]['itemTeacherName'],
                  )
              );
            },

            reverse: reversed,
            scrollDirection: Axis.vertical,
          ),

    );
  }

  Widget getExams(){

    return Container(
        height: mediaQueryData.size.height * 0.9,
//        height: md.size.height * 0.9,
        child: ListView.builder(
//          controller: controller,
          itemCount: items2.length,
          itemBuilder: (BuildContext context, int index){
            return Padding(
                padding: const EdgeInsets.all(4.0),
                child: ListTile(
                  title: Text(items2[index].toString()),

                )
            );
          },

          reverse: reversed,
          scrollDirection: Axis.vertical,
        )
    );
  }

  Widget getLabWorks(){

    return Container(
        height: mediaQueryData.size.height * 0.9,
//        height: md.size.height * 0.9,
        child: ListView.builder(
//          controller: controller,
          itemCount: items3.length,
          itemBuilder: (BuildContext context, int index){
            return Padding(
                padding: const EdgeInsets.all(4.0),
                child: ListTile(
                  title: Text(items3[index].toString()),

                )
            );
          },

          reverse: reversed,
          scrollDirection: Axis.vertical,
        )
    );
  }

}
