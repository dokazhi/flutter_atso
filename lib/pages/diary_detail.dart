import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';


class DiaryDetailSingleScore extends StatelessWidget {
  final DDSdate;
  final DDSscore;
  final DDStype;
  final DDSthemeName;


  DiaryDetailSingleScore(
    this.DDSdate,
    this.DDSscore,
    this.DDStype,
    this.DDSthemeName
  );
  final courseTypes = [
    'Лекция',
    'Семинар'
  ];
  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Text(DDSdate.toString()),
      title: Text(DDSthemeName),
      subtitle: Text(courseTypes[DDStype]),
      trailing: Text(DDSscore.toString()),
    );
  }
}


class DiaryDetail extends StatelessWidget {
  final diaryDetailName;
  final diaryDetailAvgScore;
  final List<List<dynamic>> diaryDetailListOfScores;

  DiaryDetail({
      this.diaryDetailName,
      this.diaryDetailAvgScore,
      this.diaryDetailListOfScores
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: AppBar(
        title: Text(diaryDetailName),
        backgroundColor: Colors.blueGrey,
        leading: BackButton(),
      ),
      floatingActionButton: SpeedDial(
          animatedIcon:  AnimatedIcons.menu_close,
          backgroundColor: Colors.blueGrey,
          curve: Curves.easeInCirc,
          children: [
            SpeedDialChild(
              child: Icon(Icons.weekend),
              label: 'За неделю',
              onTap: null,
            ),
            SpeedDialChild(
              child: Icon(Icons.calendar_today),
              label: 'За месяц',
              onTap: (){
              },
            ),
            SpeedDialChild(
              child: Icon(Icons.keyboard_arrow_up),
              label: 'На верх',
              onTap: (){
              },
            ),
          ]
      ),
      body: Container(
        child: ListView.builder(
          itemCount: diaryDetailListOfScores.length,
          itemBuilder: (BuildContext context, int index){
            return Padding(
              padding: const EdgeInsets.all(4.0),
              child: DiaryDetailSingleScore(
                  diaryDetailListOfScores[index][0],
                  diaryDetailListOfScores[index][1],
                  diaryDetailListOfScores[index][2],
                  diaryDetailListOfScores[index][3]
              )
            );
          }
        ),
      ),
    );
  }
}
