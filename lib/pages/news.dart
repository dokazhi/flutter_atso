import 'package:atso/components/news_article.dart';
import 'package:flutter/material.dart';


class News extends StatefulWidget {
  @override
  _NewsState createState() => _NewsState();
}

class _NewsState extends State<News> {

  var articleList = [
    {
      'articleId': 1,
      'articleTitle': 'Title 1',
      'articlePicture': 'images/1.png',
      'articleShortDescription': 'this is short description a long 20 or 30 words not more',
      'articleDescription' : '''
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Neque ornare aenean euismod elementum nisi quis eleifend quam adipiscing. Elementum sagittis vitae et leo duis ut diam quam. Et netus et malesuada fames ac turpis egestas. Egestas sed tempus urna et pharetra pharetra massa. Ac auctor augue mauris augue neque gravida in fermentum et. Amet consectetur adipiscing elit duis tristique. Mattis aliquam faucibus purus in massa tempor nec feugiat. Neque vitae tempus quam pellentesque nec nam aliquam. Nulla aliquet enim tortor at auctor urna nunc id. Bibendum arcu vitae elementum curabitur vitae nunc sed. Non odio euismod lacinia at quis risus. Elit eget gravida cum sociis natoque penatibus et magnis dis. Ultricies mi quis hendrerit dolor. Felis donec et odio pellentesque diam volutpat commodo sed. Nibh praesent tristique magna sit amet purus gravida quis. Eu non diam phasellus vestibulum lorem sed risus. Malesuada bibendum arcu vitae elementum. Vulputate enim nulla aliquet porttitor lacus luctus accumsan tortor posuere.

Est ullamcorper eget nulla facilisi etiam. Est velit egestas dui id ornare. Orci ac auctor augue mauris augue. Pellentesque pulvinar pellentesque habitant morbi tristique senectus. Gravida cum sociis natoque penatibus et magnis dis parturient. Sapien faucibus et molestie ac feugiat sed lectus vestibulum. Malesuada pellentesque elit eget gravida cum sociis. Sed risus pretium quam vulputate dignissim suspendisse in. Mi quis hendrerit dolor magna eget est lorem. Nec nam aliquam sem et. Vulputate dignissim suspendisse in est ante in nibh mauris. Pellentesque pulvinar pellentesque habitant morbi tristique senectus et netus. Ac orci phasellus egestas tellus rutrum tellus pellentesque. Non blandit massa enim nec dui. Nulla pellentesque dignissim enim sit amet venenatis urna cursus.

Posuere ac ut consequat semper viverra nam libero. Nibh sit amet commodo nulla facilisi nullam vehicula ipsum a. Nulla malesuada pellentesque elit eget gravida cum sociis natoque. Ac tortor dignissim convallis aenean et tortor at risus viverra. Posuere lorem ipsum dolor sit amet consectetur adipiscing elit. Adipiscing diam donec adipiscing tristique risus nec feugiat in. Pellentesque dignissim enim sit amet venenatis urna cursus eget. Donec ac odio tempor orci dapibus ultrices in. Nisi est sit amet facilisis magna etiam. Tempor orci dapibus ultrices in iaculis nunc sed augue lacus. Sagittis orci a scelerisque purus semper eget. Porta non pulvinar neque laoreet suspendisse interdum consectetur libero id. Rhoncus aenean vel elit scelerisque mauris pellentesque pulvinar pellentesque. Varius vel pharetra vel turpis nunc eget.
      ''',
      'articleDate': '03.02.2019 13:33'
    },
    {
      'articleId': 2,
      'articleTitle': 'Title 2',
      'articlePicture': 'images/2.png',
      'articleShortDescription': 'this is short description a long 20 or 30 words not more',
      'articleDescription' : '''
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Neque ornare aenean euismod elementum nisi quis eleifend quam adipiscing. Elementum sagittis vitae et leo duis ut diam quam. Et netus et malesuada fames ac turpis egestas. Egestas sed tempus urna et pharetra pharetra massa. Ac auctor augue mauris augue neque gravida in fermentum et. Amet consectetur adipiscing elit duis tristique. Mattis aliquam faucibus purus in massa tempor nec feugiat. Neque vitae tempus quam pellentesque nec nam aliquam. Nulla aliquet enim tortor at auctor urna nunc id. Bibendum arcu vitae elementum curabitur vitae nunc sed. Non odio euismod lacinia at quis risus. Elit eget gravida cum sociis natoque penatibus et magnis dis. Ultricies mi quis hendrerit dolor. Felis donec et odio pellentesque diam volutpat commodo sed. Nibh praesent tristique magna sit amet purus gravida quis. Eu non diam phasellus vestibulum lorem sed risus. Malesuada bibendum arcu vitae elementum. Vulputate enim nulla aliquet porttitor lacus luctus accumsan tortor posuere.

Est ullamcorper eget nulla facilisi etiam. Est velit egestas dui id ornare. Orci ac auctor augue mauris augue. Pellentesque pulvinar pellentesque habitant morbi tristique senectus. Gravida cum sociis natoque penatibus et magnis dis parturient. Sapien faucibus et molestie ac feugiat sed lectus vestibulum. Malesuada pellentesque elit eget gravida cum sociis. Sed risus pretium quam vulputate dignissim suspendisse in. Mi quis hendrerit dolor magna eget est lorem. Nec nam aliquam sem et. Vulputate dignissim suspendisse in est ante in nibh mauris. Pellentesque pulvinar pellentesque habitant morbi tristique senectus et netus. Ac orci phasellus egestas tellus rutrum tellus pellentesque. Non blandit massa enim nec dui. Nulla pellentesque dignissim enim sit amet venenatis urna cursus.

Posuere ac ut consequat semper viverra nam libero. Nibh sit amet commodo nulla facilisi nullam vehicula ipsum a. Nulla malesuada pellentesque elit eget gravida cum sociis natoque. Ac tortor dignissim convallis aenean et tortor at risus viverra. Posuere lorem ipsum dolor sit amet consectetur adipiscing elit. Adipiscing diam donec adipiscing tristique risus nec feugiat in. Pellentesque dignissim enim sit amet venenatis urna cursus eget. Donec ac odio tempor orci dapibus ultrices in. Nisi est sit amet facilisis magna etiam. Tempor orci dapibus ultrices in iaculis nunc sed augue lacus. Sagittis orci a scelerisque purus semper eget. Porta non pulvinar neque laoreet suspendisse interdum consectetur libero id. Rhoncus aenean vel elit scelerisque mauris pellentesque pulvinar pellentesque. Varius vel pharetra vel turpis nunc eget.
      ''',
      'articleDate': '03.02.2019 13:33'
    },
    {
      'articleId': 3,
      'articleTitle': 'Title 3',
      'articlePicture': 'images/3.png',
      'articleShortDescription': 'this is short description a long 20 or 30 words not more',
      'articleDescription' : '''
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Neque ornare aenean euismod elementum nisi quis eleifend quam adipiscing. Elementum sagittis vitae et leo duis ut diam quam. Et netus et malesuada fames ac turpis egestas. Egestas sed tempus urna et pharetra pharetra massa. Ac auctor augue mauris augue neque gravida in fermentum et. Amet consectetur adipiscing elit duis tristique. Mattis aliquam faucibus purus in massa tempor nec feugiat. Neque vitae tempus quam pellentesque nec nam aliquam. Nulla aliquet enim tortor at auctor urna nunc id. Bibendum arcu vitae elementum curabitur vitae nunc sed. Non odio euismod lacinia at quis risus. Elit eget gravida cum sociis natoque penatibus et magnis dis. Ultricies mi quis hendrerit dolor. Felis donec et odio pellentesque diam volutpat commodo sed. Nibh praesent tristique magna sit amet purus gravida quis. Eu non diam phasellus vestibulum lorem sed risus. Malesuada bibendum arcu vitae elementum. Vulputate enim nulla aliquet porttitor lacus luctus accumsan tortor posuere.

Est ullamcorper eget nulla facilisi etiam. Est velit egestas dui id ornare. Orci ac auctor augue mauris augue. Pellentesque pulvinar pellentesque habitant morbi tristique senectus. Gravida cum sociis natoque penatibus et magnis dis parturient. Sapien faucibus et molestie ac feugiat sed lectus vestibulum. Malesuada pellentesque elit eget gravida cum sociis. Sed risus pretium quam vulputate dignissim suspendisse in. Mi quis hendrerit dolor magna eget est lorem. Nec nam aliquam sem et. Vulputate dignissim suspendisse in est ante in nibh mauris. Pellentesque pulvinar pellentesque habitant morbi tristique senectus et netus. Ac orci phasellus egestas tellus rutrum tellus pellentesque. Non blandit massa enim nec dui. Nulla pellentesque dignissim enim sit amet venenatis urna cursus.

Posuere ac ut consequat semper viverra nam libero. Nibh sit amet commodo nulla facilisi nullam vehicula ipsum a. Nulla malesuada pellentesque elit eget gravida cum sociis natoque. Ac tortor dignissim convallis aenean et tortor at risus viverra. Posuere lorem ipsum dolor sit amet consectetur adipiscing elit. Adipiscing diam donec adipiscing tristique risus nec feugiat in. Pellentesque dignissim enim sit amet venenatis urna cursus eget. Donec ac odio tempor orci dapibus ultrices in. Nisi est sit amet facilisis magna etiam. Tempor orci dapibus ultrices in iaculis nunc sed augue lacus. Sagittis orci a scelerisque purus semper eget. Porta non pulvinar neque laoreet suspendisse interdum consectetur libero id. Rhoncus aenean vel elit scelerisque mauris pellentesque pulvinar pellentesque. Varius vel pharetra vel turpis nunc eget.
      ''',
      'articleDate': '03.02.2019 13:33'
    },
    {
      'articleId': 4,
      'articleTitle': 'Title 4',
      'articlePicture': 'images/4.png',
      'articleShortDescription': 'this is short description a long 20 or 30 words not more',
      'articleDescription' : '''
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Neque ornare aenean euismod elementum nisi quis eleifend quam adipiscing. Elementum sagittis vitae et leo duis ut diam quam. Et netus et malesuada fames ac turpis egestas. Egestas sed tempus urna et pharetra pharetra massa. Ac auctor augue mauris augue neque gravida in fermentum et. Amet consectetur adipiscing elit duis tristique. Mattis aliquam faucibus purus in massa tempor nec feugiat. Neque vitae tempus quam pellentesque nec nam aliquam. Nulla aliquet enim tortor at auctor urna nunc id. Bibendum arcu vitae elementum curabitur vitae nunc sed. Non odio euismod lacinia at quis risus. Elit eget gravida cum sociis natoque penatibus et magnis dis. Ultricies mi quis hendrerit dolor. Felis donec et odio pellentesque diam volutpat commodo sed. Nibh praesent tristique magna sit amet purus gravida quis. Eu non diam phasellus vestibulum lorem sed risus. Malesuada bibendum arcu vitae elementum. Vulputate enim nulla aliquet porttitor lacus luctus accumsan tortor posuere.

Est ullamcorper eget nulla facilisi etiam. Est velit egestas dui id ornare. Orci ac auctor augue mauris augue. Pellentesque pulvinar pellentesque habitant morbi tristique senectus. Gravida cum sociis natoque penatibus et magnis dis parturient. Sapien faucibus et molestie ac feugiat sed lectus vestibulum. Malesuada pellentesque elit eget gravida cum sociis. Sed risus pretium quam vulputate dignissim suspendisse in. Mi quis hendrerit dolor magna eget est lorem. Nec nam aliquam sem et. Vulputate dignissim suspendisse in est ante in nibh mauris. Pellentesque pulvinar pellentesque habitant morbi tristique senectus et netus. Ac orci phasellus egestas tellus rutrum tellus pellentesque. Non blandit massa enim nec dui. Nulla pellentesque dignissim enim sit amet venenatis urna cursus.

Posuere ac ut consequat semper viverra nam libero. Nibh sit amet commodo nulla facilisi nullam vehicula ipsum a. Nulla malesuada pellentesque elit eget gravida cum sociis natoque. Ac tortor dignissim convallis aenean et tortor at risus viverra. Posuere lorem ipsum dolor sit amet consectetur adipiscing elit. Adipiscing diam donec adipiscing tristique risus nec feugiat in. Pellentesque dignissim enim sit amet venenatis urna cursus eget. Donec ac odio tempor orci dapibus ultrices in. Nisi est sit amet facilisis magna etiam. Tempor orci dapibus ultrices in iaculis nunc sed augue lacus. Sagittis orci a scelerisque purus semper eget. Porta non pulvinar neque laoreet suspendisse interdum consectetur libero id. Rhoncus aenean vel elit scelerisque mauris pellentesque pulvinar pellentesque. Varius vel pharetra vel turpis nunc eget.
      ''',
      'articleDate': '03.02.2019 13:33'
    },
    {
      'articleId': 5,
      'articleTitle': 'Title 5',
      'articlePicture': 'images/5.png',
      'articleShortDescription': 'this is short description a long 20 or 30 words not more',
      'articleDescription' : '''
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Neque ornare aenean euismod elementum nisi quis eleifend quam adipiscing. Elementum sagittis vitae et leo duis ut diam quam. Et netus et malesuada fames ac turpis egestas. Egestas sed tempus urna et pharetra pharetra massa. Ac auctor augue mauris augue neque gravida in fermentum et. Amet consectetur adipiscing elit duis tristique. Mattis aliquam faucibus purus in massa tempor nec feugiat. Neque vitae tempus quam pellentesque nec nam aliquam. Nulla aliquet enim tortor at auctor urna nunc id. Bibendum arcu vitae elementum curabitur vitae nunc sed. Non odio euismod lacinia at quis risus. Elit eget gravida cum sociis natoque penatibus et magnis dis. Ultricies mi quis hendrerit dolor. Felis donec et odio pellentesque diam volutpat commodo sed. Nibh praesent tristique magna sit amet purus gravida quis. Eu non diam phasellus vestibulum lorem sed risus. Malesuada bibendum arcu vitae elementum. Vulputate enim nulla aliquet porttitor lacus luctus accumsan tortor posuere.

Est ullamcorper eget nulla facilisi etiam. Est velit egestas dui id ornare. Orci ac auctor augue mauris augue. Pellentesque pulvinar pellentesque habitant morbi tristique senectus. Gravida cum sociis natoque penatibus et magnis dis parturient. Sapien faucibus et molestie ac feugiat sed lectus vestibulum. Malesuada pellentesque elit eget gravida cum sociis. Sed risus pretium quam vulputate dignissim suspendisse in. Mi quis hendrerit dolor magna eget est lorem. Nec nam aliquam sem et. Vulputate dignissim suspendisse in est ante in nibh mauris. Pellentesque pulvinar pellentesque habitant morbi tristique senectus et netus. Ac orci phasellus egestas tellus rutrum tellus pellentesque. Non blandit massa enim nec dui. Nulla pellentesque dignissim enim sit amet venenatis urna cursus.

Posuere ac ut consequat semper viverra nam libero. Nibh sit amet commodo nulla facilisi nullam vehicula ipsum a. Nulla malesuada pellentesque elit eget gravida cum sociis natoque. Ac tortor dignissim convallis aenean et tortor at risus viverra. Posuere lorem ipsum dolor sit amet consectetur adipiscing elit. Adipiscing diam donec adipiscing tristique risus nec feugiat in. Pellentesque dignissim enim sit amet venenatis urna cursus eget. Donec ac odio tempor orci dapibus ultrices in. Nisi est sit amet facilisis magna etiam. Tempor orci dapibus ultrices in iaculis nunc sed augue lacus. Sagittis orci a scelerisque purus semper eget. Porta non pulvinar neque laoreet suspendisse interdum consectetur libero id. Rhoncus aenean vel elit scelerisque mauris pellentesque pulvinar pellentesque. Varius vel pharetra vel turpis nunc eget.
      ''',
      'articleDate': '03.02.2019 13:33'
    },
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Новости'),
        backgroundColor: Colors.blueGrey,
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.keyboard_arrow_up),
        onPressed: (){},
        backgroundColor: Colors.blueGrey,
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 8.0),
        child: GridView.builder(
            itemCount: articleList.length,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 1),
            itemBuilder: (BuildContext context, int index){
              return Padding(
                padding: const EdgeInsets.all(4.0),
                child: Article(
                    articleId: articleList[index]['articleId'],
                    articleTitle: articleList[index]['articleTitle'],
                    articlePicture: articleList[index]['articlePicture'],
                    articleShortDescription: articleList[index]['articleShortDescription'],
                    articleDescription: articleList[index]['articleDescription'],
                    articleDate: articleList[index]['articleDate']
                ),
              );
            }
        ),
      ),
    );
  }
}
