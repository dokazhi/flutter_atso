import 'package:atso/components/schedule_day.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class Schedule extends StatefulWidget {
  @override
  _ScheduleState createState() => _ScheduleState();
}

class _ScheduleState extends State<Schedule> {
  List<String> daysOfWeek = [
    'Понедельник',
    'Вторник',
    'Среда',
    'Четверг',
    'Пятница',
    'Суббота'
  ];
  var scheduleDays = [];
  @override
  void initState() {
    getScheduleDaysWidgets();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text('Расписание'),
        backgroundColor: Colors.blueGrey,
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.blueGrey,
        onPressed: (){},
        child: Icon(Icons.keyboard_arrow_up),
      ),
      body: ListView.builder(
            itemCount: daysOfWeek.length,
            itemBuilder: (BuildContext context, int index){
              return Padding(
                padding: const EdgeInsets.all(4.0),
                child: ScheduleDay(
                  scheduleWeekDay: daysOfWeek[index],
                  width: size.width ,
                  height: size.height,
                )
              );
            }
        ),
    );
  }

  List<Column> getScheduleDaysWidgets() {
    List<Column> result = [];
    var daysOfWeek = [
      'Понедельник',
      'Вторник',
      'Среда',
      'Четверг',
      'Пятница',
      'Суббота'
    ];
    daysOfWeek.forEach((element) {
      result.add(ScheduleDay(scheduleWeekDay: element,).build(context));
    });

    return result;
  }
}
