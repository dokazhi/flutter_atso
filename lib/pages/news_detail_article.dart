import 'package:flutter/material.dart';


class NewsDetailBody extends StatelessWidget {
  final picturePath;
  final description;
  final date;
  final title;
  NewsDetailBody({this.picturePath, this.description, this.date, this.title});
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListView(
          children: [
            Container(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Image.asset(picturePath),
              ),
              decoration: BoxDecoration(
                border: Border.all(color: Colors.lightBlueAccent, width: 5, style: BorderStyle.solid)
              ),
            ),
            Center(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(title, style: TextStyle(color: Colors.blueAccent, fontWeight: FontWeight.w800, fontSize: 20,),),),
            ),
            Center(child: Text(date, style: TextStyle(color: Colors.black54, fontWeight: FontWeight.w600, fontSize: 16),),),
            Divider(thickness: 1.0,),
            Padding(
              padding: const EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
              child: Text(description, textAlign: TextAlign.justify, style: TextStyle(fontSize: 16,), softWrap: true,),),
          ],
        ),
      ),
    );
  }
}




class NewsDetailPage extends StatefulWidget {

  final articleId;
  final articleTitle;
  final articlePicture;
  final articleDescription;
  final articleDate;

  NewsDetailPage({
    this.articleId,
    this.articleTitle,
    this.articlePicture,
    this.articleDescription,
    this.articleDate,
  });

  @override
  _NewsDetailPageState createState() => _NewsDetailPageState(
    articleId: this.articleId,
    articleTitle: this.articleTitle,
    articleDescription: this.articleDescription,
    articleDate: this.articleDate,
    articlePicture: this.articlePicture
  );
}

class _NewsDetailPageState extends State<NewsDetailPage> {
  final articleId;
  final articleTitle;
  final articlePicture;
  final articleDescription;
  final articleDate;

  _NewsDetailPageState({
    this.articleId,
    this.articleTitle,
    this.articlePicture,
    this.articleDescription,
    this.articleDate,
  });

  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: articleId.toString(),
      child: Material(
//      appBar: AppBar(
//        title: Text(articleTitle),
//      ),
        child: NewsDetailBody(
          picturePath: articlePicture,
          description: articleDescription,
          date: articleDate,
          title: articleTitle
        ),
      ),
    );
  }
}
