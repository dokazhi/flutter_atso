import 'package:atso/pages/diary.dart';
import 'package:atso/pages/exams.dart';
import 'package:atso/pages/news.dart';
import 'package:atso/pages/profile.dart';
import 'package:atso/pages/schedule.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:bottom_navy_bar/bottom_navy_bar.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int _selectedIndex = 0;
  PageController _pageController;
  @override
  void initState() {
    super.initState();
    _pageController = PageController();
  }
  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var mediaQueryData = MediaQuery.of(context);
    return Scaffold(
      bottomNavigationBar: BottomNavyBar(
        selectedIndex: _selectedIndex,
        showElevation: true, // use this to remove appBar's elevation
        onItemSelected: (index) => setState(() {
          _selectedIndex = index;
          _pageController.animateToPage(index,
              duration: Duration(milliseconds: 300), curve: Curves.ease);
        }),
        items: [
          BottomNavyBarItem(
            icon: Icon(CupertinoIcons.news, size: 28.0,),
            title: Text('Новости', style: TextStyle(color: Colors.blueGrey),),
            inactiveColor: Colors.blue,
            activeColor: Colors.blueGrey,
          ),
          BottomNavyBarItem(
              icon: Icon(CupertinoIcons.time, size: 28.0,),
              title: Text('Расписание', style: TextStyle(color: Colors.blueGrey),),
              inactiveColor: Colors.blue,
              activeColor: Colors.blueGrey
          ),
          BottomNavyBarItem(
              icon: Icon(CupertinoIcons.book, size: 28.0,),
              title: Text('Дневник', style: TextStyle(color: Colors.blueGrey),),
              inactiveColor: Colors.blue,
              activeColor: Colors.blueGrey
          ),
          BottomNavyBarItem(
              icon: Icon(CupertinoIcons.lab_flask, size: 28.0,),
              title: Text('Контроль', style: TextStyle(color: Colors.blueGrey),),
              inactiveColor: Colors.blue,
              activeColor: Colors.blueGrey
          ),
          BottomNavyBarItem(
              icon: Icon(CupertinoIcons.profile_circled, size: 28.0,),
              title: Text('Профиль', style: TextStyle(color: Colors.blueGrey),),
              inactiveColor: Colors.blue,
              activeColor: Colors.blueGrey
          ),
        ],
      ),
      body:SizedBox.expand(
        child: PageView(
          controller: _pageController,
          onPageChanged: (index) {
            setState(() => _selectedIndex = index);
          },
          children: <Widget>[
            News(),
            Schedule(),
            Diary(),
            Exams(mediaQueryData: mediaQueryData,),
            Profile(mediaQueryData: mediaQueryData),
          ],
        ),
      ),
    );
  }
}
