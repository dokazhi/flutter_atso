import 'package:atso/pages/transcript.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';


class ProfileGPA extends StatelessWidget {
  MediaQueryData mediaQueryData;
  ProfileGPA({
    this.mediaQueryData
  });
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        width: mediaQueryData.size.width * 0.92,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
              child: Center(
                child: FittedBox(
                  child: Text(
                    'GPA',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 40,
                      fontWeight: FontWeight.w800
                    ),
                  ),
                ),
              ),
            ),

          Padding(
            padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
            child: Center(
              child: FittedBox(
                child: Text(
                  '3.72',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.green,
                    fontSize: 52,
                    fontWeight: FontWeight.w800
                  ),
                ),
              ),
            ),
          )
          ],
        ),
      ),
    );
  }
}

class ProfileContacts extends StatelessWidget {
  MediaQueryData mediaQueryData;
  ProfileContacts({
    this.mediaQueryData
  });
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(12.0, 0, 0.0, 0),
      child: Container(
        width: mediaQueryData.size.width * 0.92,
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Center(
                child: Text(
                  'Конакты',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 18
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 8.0, 0, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      child:Text(
                        'Телефон куратора',
                        textAlign: TextAlign.left,
                        style: TextStyle(fontSize: 14),
                      )
                    ),
                    Expanded(
                      child: Text(
                        '+7 (777) 333 22 11',
                        textAlign: TextAlign.right,
                        style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 8.0, 0, 0),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Text(
                        'Телефон офис-регистратуры',
                        textAlign: TextAlign.left,
                        style: TextStyle(fontSize: 14),
                      ),
                    ),
                    Expanded(
                      child: Text(
                        '+7 (727) 279 95 70',
                        textAlign: TextAlign.right,
                        style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 8.0, 0, 0),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Text(
                        'Телефон офис-регистратуры',
                        textAlign: TextAlign.left,
                        style: TextStyle(fontSize: 14),
                      ),
                    ),
                    Expanded(
                      child: Text(
                        '+7 (727) 279 95 43',
                        textAlign: TextAlign.right,
                        style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
                      ),
                    )
                  ],
                ),
              ),
            ],
        ),
      ),
    );
  }
}



class ProfileImage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 4,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(14.0, 0, 14.0, 0),
        child: Image.asset('images/atso.png'),
      ),
    );
  }
}

class ProfileInfo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 6,
      child: Padding(
        padding: const EdgeInsets.only(left:8.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.fromLTRB(18.0, 8.0, 0, 2),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Icon(Icons.person_pin),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text('Кажикумаров Д.Е.', style: TextStyle(fontWeight: FontWeight.w600, fontSize: 18.0),),
                        Text('ФИО', style: TextStyle(color: Colors.black54, fontSize: 14),)
                      ],
                    ),
                  )
                ],
              ),
            ),

            Padding(
              padding: const EdgeInsets.fromLTRB(18.0, 8.0, 0, 2),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Icon(Icons.group),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text('МагВТиПО-19', style: TextStyle(fontWeight: FontWeight.w600, fontSize: 18.0),),
                        Text('Группа', style: TextStyle(color: Colors.black54, fontSize: 14),)
                      ],
                    ),
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(18.0, 8.0, 0, 2),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Icon(Icons.event),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text('Очное', style: TextStyle(fontWeight: FontWeight.w600, fontSize: 18.0),),
                        Text('Форма обучения', style: TextStyle(color: Colors.black54, fontSize: 14),)
                      ],
                    ),
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(18.0, 8.0, 0, 2),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Icon(Icons.person),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text('941128301479', style: TextStyle(fontWeight: FontWeight.w600, fontSize: 18.0),),
                        Text('ИИН', style: TextStyle(color: Colors.black54, fontSize: 14),)
                      ],
                    ),
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(18.0, 8.0, 0, 2),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Icon(Icons.person_outline),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text('Ташев А.А.', style: TextStyle(fontWeight: FontWeight.w600, fontSize: 18.0),),
                        Text('Куратор', style: TextStyle(color: Colors.black54, fontSize: 14),)
                      ],
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      )
    );
  }
}



class ProfileBody extends StatelessWidget {
  MediaQueryData mediaQueryData;
  ProfileBody({
    this.mediaQueryData
  });
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            ProfileImage(),
            ProfileInfo(),
          ],
        ),
        Divider(
          thickness: 2,
        ),
        Row(
          children: <Widget>[
            ProfileContacts(mediaQueryData: mediaQueryData)
          ],
        ),
        Divider(
          thickness: 2,
        ),
        Row(
          children: <Widget>[
            ProfileGPA(mediaQueryData: mediaQueryData)
          ],
        ),
      ],
    );
  }
}


class Profile extends StatefulWidget {

  MediaQueryData mediaQueryData;
  Profile({
    this.mediaQueryData
  });
  @override
  _ProfileState createState() => _ProfileState(mediaQueryData: mediaQueryData);
}

class _ProfileState extends State<Profile> {
  List courses = [
        {
          'avgGPA': 93,
          'items': [
            {
              'name': 'Математика',
              'gpa': 3.77,
              'score': 93
            },
            {
              'name': 'Высшая математика',
              'gpa': 3.77,
              'score': 93
            },
            {
              'name': 'Физика',
              'gpa': 3.77,
              'score': 93
            },
          ]
        },
        {
          'avgGPA': 93,
          'items': [
            {
              'name': 'Математика S',
              'gpa': 3.77,
              'score': 93
            },
            {
              'name': 'Высшая математика S',
              'gpa': 3.67,
              'score': 93
            },
            {
              'name': 'Физика S',
              'gpa': 3.78,
              'score': 93
            },
          ]
        },
        {
          'avgGPA': 93,
          'items': [
            {
              'name': 'Математика XX',
              'gpa': 3.77,
              'score': 93
            },
            {
              'name': 'Высшая математика XX',
              'gpa': 3.77,
              'score': 93
            },
            {
              'name': 'Физика XX',
              'gpa': 3.77,
              'score': 93
            },
          ]
        },
        {
          'avgGPA': 92,
          'items': [
            {
              'name': 'Математика XXS',
              'gpa': 3.77,
              'score': 93
            },
            {
              'name': 'Высшая математика XXS',
              'gpa': 3.67,
              'score': 93
            },
            {
              'name': 'Физика XXS',
              'gpa': 3.78,
              'score': 93
            },
          ]
        },
    {
      'avgGPA': 92,
      'items': [
        {
          'name': 'Математика XXS',
          'gpa': 3.77,
          'score': 93
        },
        {
          'name': 'Высшая математика XXS',
          'gpa': 3.67,
          'score': 93
        },
        {
          'name': 'Физика XXS',
          'gpa': 3.78,
          'score': 93
        },
      ]
    },
    {
      'avgGPA': 92,
      'items': [
        {
          'name': 'Математика XXS',
          'gpa': 3.77,
          'score': 93
        },
        {
          'name': 'Высшая математика XXS',
          'gpa': 3.67,
          'score': 93
        },
        {
          'name': 'Физика XXS',
          'gpa': 3.78,
          'score': 93
        },
      ]
    },
    {
      'avgGPA': 92,
      'items': [
        {
          'name': 'Математика XXS',
          'gpa': 3.77,
          'score': 93
        },
        {
          'name': 'Высшая математика XXS',
          'gpa': 3.67,
          'score': 93
        },
        {
          'name': 'Физика XXS',
          'gpa': 3.78,
          'score': 93
        },
      ]
    },
    {
      'avgGPA': 92,
      'items': [
        {
          'name': 'Математика XXS',
          'gpa': 3.77,
          'score': 93
        },
        {
          'name': 'Высшая математика XXS',
          'gpa': 3.67,
          'score': 93
        },
        {
          'name': 'Физика XXS',
          'gpa': 3.78,
          'score': 93
        },
      ]
    },
  ];
  MediaQueryData mediaQueryData;
  _ProfileState({
    this.mediaQueryData
  });
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Профиль'),
        backgroundColor: Colors.blueGrey,
        actions: <Widget>[
          IconButton(
            icon: FaIcon(FontAwesomeIcons.addressCard),
            onPressed: () => Navigator.of(context).push(
              new MaterialPageRoute(builder: (context) => TranscriptPage(courses: courses,))
            ),
          )
        ],
      ),

      body: ProfileBody(
        mediaQueryData: mediaQueryData
      )
    );
  }
}
