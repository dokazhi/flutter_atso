import 'package:atso/pages/diary_detail.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class DiaryItem extends StatelessWidget {
  final itemName;
  final itemAvgScore;

  DiaryItem({this.itemName, this.itemAvgScore});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListTile(
        onTap: ()=> Navigator.of(
            context
        ).push(
            new MaterialPageRoute(
                builder: (context) => DiaryDetail(
                    diaryDetailName: itemName,
                    diaryDetailAvgScore: itemAvgScore,
                    diaryDetailListOfScores: [
                      ['12.12.2020', 93, 0, 'Понятия теории множеств'],
                      ['12.12.2020', 93, 0, 'Комплексные числа'],
                      ['12.12.2020', 93, 0, 'Логарифмы'],
                      ['12.12.2020', 93, 0, 'Производные'],
                      ['12.12.2020', 93, 0, 'Интегралы'],
                      ['12.12.2020', 93, 0, 'Интегралы в геометрии'],
                      ['12.12.2020', 93, 0, 'Двойные интегралы'],
                      ['12.12.2020', 93, 0, 'Матрицы'],
                    ]
                )
            )
        ),
        title: Text(itemName),
        subtitle: Text('GPA: $itemAvgScore'),
        trailing: Icon(Icons.arrow_forward_ios),
      ),
    );
  }
}


class Diary extends StatefulWidget {
  @override
  _DiaryState createState() => _DiaryState();
}

class _DiaryState extends State<Diary> {
  var diaryItems = [
    {
      'itemName': 'Математика',
      'itemAvgScore': 3.33
    },
    {
      'itemName': 'Физика',
      'itemAvgScore': 3.63
    },
    {
      'itemName': 'Алгоритмизация',
      'itemAvgScore': 3.1
    },
    {
      'itemName': 'Моделирование',
      'itemAvgScore': 3.13
    },
    {
      'itemName': 'Численные методы',
      'itemAvgScore': 3.5
    },
    {
      'itemName': 'Математика2',
      'itemAvgScore': 3.6
    },
    {
      'itemName': 'Математика3',
      'itemAvgScore': 3.7
    },
    {
      'itemName': 'Математика4',
      'itemAvgScore': 3.8
    },
    {
      'itemName': 'Математика5',
      'itemAvgScore': 3.3
    },
    {
      'itemName': 'Математика6',
      'itemAvgScore': 4.0
    },

  ];
  @override
  Widget build(BuildContext context) {
    var md = MediaQuery.of(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Дневник'),
        backgroundColor: Colors.blueGrey,
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.keyboard_arrow_up),
        onPressed: (){},
        backgroundColor: Colors.blueGrey,
      ),
      body: Container(
        child: ListView.builder(
            itemCount: diaryItems.length,
            itemBuilder: (BuildContext context, int index){
              return Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: DiaryItem(
                    itemName: diaryItems[index]['itemName'],
                    itemAvgScore: diaryItems[index]['itemAvgScore']
                  )
              );
            }
          ),
      ),
    );
  }
}
